/*
 * main.cpp
 *
 *  Created on: 11/08/2018
 *      Author: viridiancore
 */

#include <iostream>
#include "alignment/smithwaterman.hpp"

int main() {
	std::cout << "test";



	base target[] = "TCG";
	base reference[] = "ACTCGGTCG";  // arr is in references

	alignment::Sequence seq(target);
	alignment::ReferenceSequence ref(reference);

	alignment::SmithWaterman alignment_search (ref);
	alignment_search.sequenceFind(seq);



	std::cout << "\nOperation Completed Successfully.\n";
}
