/*
 * smithwaterman.cpp
 *
 *  Created on: 11/08/2018
 *      Author: viridiancore
 */

#include <vector>
#include <algorithm>  // for std::max_element

#include <iostream>

#include "baseclasses.hpp"
#include "smithwaterman.hpp"

// source (11/08/2018)
// https://en.wikipedia.org/wiki/Smith-Waterman_algorithm

alignment::SearchResults alignment::SmithWaterman::sequenceFind(Sequence& seq) {
	// these values are used a lot. probably going to be optimised into register anyway
	register uint32 seq_length = seq.length;
	register uint32 ref_length = ref.length;

	// allocate all matricies
	matrix_size = (seq_length+1)*(ref_length+1);
	std::vector<base> scoring_matrix (matrix_size);
	std::vector<uint32> matrix2 (matrix_size);  // Ii
	std::vector<uint32> matrix3 (matrix_size);  // Ij

	// (2) initialise first row and column of scoring matrix as 0.
	// going to set the whole thing to zero to prevent junk data from being used by anything.
	for (auto i = 0; i < matrix_size; i++) scoring_matrix[i] = 0;

	// (3) fill scoring matrix as per the equation. let i = target sequence index, j = reference sequence index
	std::vector<float> values = { 0, 0, 0, 0 };  // the maximum of "values" is applied to the scoring matrix
	auto max_index = std::distance(values.begin(), std::max_element(values.begin(), values.end()));  // declaring outside of loop

	for (uint32 i = 1; i < seq_length; i++) {
		for (uint32 j = 1; j < ref_length; j++ ) {
			// todo this may all be fucked up, should i be using (i-n)*ref_length instead?
			values[0] = scoring_matrix[(i-1)*seq_length+j-1] + getSimilarity(seq.sequence[i], ref.sequence[j]);
			values[1] = scoring_matrix[(i-1)*seq_length+j] + penalty; // scoring_matrix[i-1][j] + penalty;
			values[2] = scoring_matrix[i*seq_length+j-1] + penalty; // scoring_matrix[i][j-1] + penalty;
			// values[3] is always zero

			max_index = std::distance(values.begin(), std::max_element(values.begin(), values.end()));  // get index of the maximum of "values"
			scoring_matrix[i*seq_length+j] = values[max_index]; //*std::max_element(values.begin(), values.end());

			std::cout << max_index << std::endl;


			/*
			switch (max_index) {
			case 0:
				matrix2[i*seq_length+j]= i - 1;
				matrix3[i*seq_length+j] = j - 1;
				break;
			case 1:
				matrix2[i*seq_length+j]= i - 1;
				matrix3[i*seq_length+j] = j;
				break;
			case 2:
				matrix2[i*seq_length+j]= i;
				matrix3[i*seq_length+j] = j - 1;
				break;
			case 3:
				matrix2[i*seq_length+j]= i;
				matrix3[i*seq_length+j] = j;
				break;
			}


*/
			// find maximum of scoring_matrix
			uint32 i_max = 0;
			uint32 j_max = 0;


		}
	}




	std::vector<uint32> indicies;  // contains indicies of located matches

	//



	//https://github.com/ngopal/SimpleSmithWatermanCPP/blob/master/SmithWaterman.cpp


}
