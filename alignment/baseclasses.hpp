/*
 * baseclasses.hpp
 *
 *  Created on: 11/08/2018
 *      Author: viridiancore
 */

#ifndef ALIGNMENT_BASECLASSES_HPP_
#define ALIGNMENT_BASECLASSES_HPP_

#include <cstring>

typedef char base;
typedef unsigned int uint32;

namespace alignment {

	struct Sequence { // target sequence
		base* sequence;
		uint32 length;
		Sequence() : sequence(nullptr), length(0) {}  // trivial constructor
		Sequence(base* seq) : sequence(seq) { // constructor for an existing array of bases
			length = strlen(seq);  // todo error handling for
		}
	};

	struct ReferenceSequence : public Sequence {  // reference sequence
		// base* sequence;  // inherited from Sequence
		ReferenceSequence() : Sequence() {}  // trivial constructor
		ReferenceSequence(base* seq) : Sequence(seq) {}  // existing array of bases
	};

	class SearchResults {
	public:
		SearchResults();
	};

	class AlignmentTool {
	protected:
		ReferenceSequence& ref;
	public:
		AlignmentTool(ReferenceSequence ref) : ref(ref) {}
		virtual ~AlignmentTool() {}
		virtual SearchResults sequenceFind(Sequence& seq) = 0;

	};

}



#endif /* ALIGNMENT_BASECLASSES_HPP_ */
