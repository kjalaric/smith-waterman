/*
 * smithwaterman.hpp
 *
 *  Created on: 11/08/2018
 *      Author: viridiancore
 */

#ifndef ALIGNMENT_SMITHWATERMAN_HPP_
#define ALIGNMENT_SMITHWATERMAN_HPP_

#include <memory>

#include "baseclasses.hpp"


namespace alignment {

	namespace constants {
		const float DEFAULT_SW_PENALTY = -3.0;  // default Smith-Waterman penalty value
	}

	class SmithWaterman : public AlignmentTool {
	protected:
		float penalty;
		// base* matrix1;
		uint32 matrix_size;

		float getSimilarity(base a, base b) { return ((a == b) ? 1.0 : penalty); }  // return 1 if they are the same, penalty otherwise
	public:
		SmithWaterman(ReferenceSequence& ref, float penalty=constants::DEFAULT_SW_PENALTY) :
			AlignmentTool(ref), penalty(penalty)
			{}
		virtual ~SmithWaterman() {}
		void updatePenalty(float new_penalty) { penalty = new_penalty; }
		SearchResults sequenceFind(Sequence& seq);


	};
}




#endif /* ALIGNMENT_SMITHWATERMAN_HPP_ */
